import Controller from '@ember/controller';
import {computed} from '@ember/object'


export default Controller.extend({
    isDone: false,
    novoModel: null,
    current: '',
  
    actions: {
      editTask(task) {
        this.transitionToRoute('task.edit', task.get('id'));
      },
      toggleTask(task) {
        let isDone = task.get('isDone');
        task.set('isDone', !isDone);
        console.log(task);
        task.save();
      },
      taskStatus(task){
        return !task.get('isDone');

      },

      createModel(){
        this.novoModel= this.get('model');
      },
      showAll(){

        this.set('model', this.get('alltesk'));
        this.save();
      },
      showCompleted(){
        console.log(this.get('model'));
        let model = this.get('model');
        this.set('model', model.filter(task=>{
          return task.isDone;
        }));
       this.save();
      },
      showIncomplete(){
        console.log(this.get('model'));
        let model = this.get('model');
        this.set('alltesk',model);
        this.set('model', model.filter(task=>{
          return !task.isDone;
        }));
        this.save();
      }
    }
});