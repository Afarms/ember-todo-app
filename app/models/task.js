//import Model from '@ember-data/model';
import DS from 'ember-data';


export default DS.Model.extend({
    description: DS.attr('string'),
    deadline: DS.attr('string'),
    isDone: DS.attr('boolean', { defaultValue: false })
  });

 /* 
export default class TaskModel extends Model {
   description =  String;
   deadline= String;
   isDone = Boolean= { defaultValue: false };
}
*/
